//
//  InterfaceController.h
//  SmartCarProj WatchKit Extension
//
//  Created by Siddhant Dange on 3/27/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface SettingsController : WKInterfaceController

@end
