//
//  User.m
//  SmartCarProj
//
//  Created by Siddhant Dange on 4/2/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "User.h"
#import "APIConnector.h"

static User *gInstance;

@interface User ()

@property (nonatomic, strong) APIConnector *apiConnector;
@property (nonatomic, strong) NSArray *vins;
@property (nonatomic, strong) NSString *selectedVin;
@property (nonatomic, assign) BOOL carHasRangeExtender;

@end

@implementation User




-(NSArray *)vins{
    return _vins;
}

-(NSString *)selectedVin{
    return _selectedVin;
}

-(void)selectVin:(NSString *)vin{
    if ([_vins containsObject:vin]){
        _selectedVin = vin;
    }
}

//USING SELECTED VIN

#pragma -mark UsingSelectedVin

-(void)getCarLocationWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    
    [_apiConnector requestMethod:@"GET" withPath:[NSString stringWithFormat:@"vehicles/%@/location", vin] andData:nil completion:^(NSDictionary *data) {
        completion(data);
    }];
}

-(void)getCarFuelLevelWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    if(_carHasRangeExtender){
        
    }
    [_apiConnector requestMethod:@"GET" withPath:[NSString stringWithFormat:@"vehicles/%@/battery", vin] andData:nil completion:^(NSDictionary *data) {
        NSMutableDictionary *passedData = @{}.mutableCopy;
        if(![data .allKeys containsObject:@"error"]){
            [passedData setObject:data[@"remainingPercent"] forKey:@"battery_percentage"];
        }
        
        [_apiConnector requestMethod:@"GET" withPath:[NSString stringWithFormat:@"vehicles/%@/fuel", vin] andData:nil completion:^(NSDictionary *data) {
            if(![data .allKeys containsObject:@"error"]){
                [passedData setObject:data[@"remainingPercent"] forKey:@"fuel_percentage"];
            }
            
            completion(passedData);
        }];
    }];
}

-(void)honkCarHornWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    [_apiConnector requestMethod:@"POST" withPath:[NSString stringWithFormat:@"vehicles/%@/horn", vin] andData:nil completion:completion];
}

-(void)lockStatusOfCarWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    [_apiConnector requestMethod:@"GET" withPath:[NSString stringWithFormat:@"vehicles/%@/doors", vin] andData:nil completion:^(NSDictionary *data) {
        BOOL lockStatus = data[@"isVehicleLocked"];
        NSDictionary *passedData = @{
                                     @"lock_status" : @(lockStatus)
                                     };
        completion(passedData);
    }];
}

-(void)lockCarWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    [_apiConnector requestMethod:@"POST" withPath:[NSString stringWithFormat:@"vehicles/%@/lock", vin] andData:nil completion:completion];
}

-(void)flickCarHeadlightsWithCompletion:(void(^)(NSDictionary*))completion{
    NSString *vin = [self selectedVin];
    [_apiConnector requestMethod:@"POST" withPath:[NSString stringWithFormat:@"vehicles/%@/lights", vin] andData:nil completion:completion];
}

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gInstance = [[User alloc] init];
        
        //init api connector
        gInstance.apiConnector = [[APIConnector alloc] init];
        
        //sync vins
        NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.SmartCarProj"];
        [mySharedDefaults synchronize];
        
        //set to user instance
        gInstance.vins = [NSArray arrayWithArray:[mySharedDefaults arrayForKey:@"vins"]];
        if(gInstance.vins.count){
            gInstance.selectedVin = gInstance.vins[0];
        }
    });
            
    return gInstance;
}
@end
