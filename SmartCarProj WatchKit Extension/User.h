//
//  User.h
//  SmartCarProj
//
//  Created by Siddhant Dange on 4/2/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

-(NSArray*)vins;
-(NSString*)selectedVin;

-(void)selectVin:(NSString*)vin;

-(void)getCarLocationWithCompletion:(void(^)(NSDictionary*))completion;
-(void)getCarFuelLevelWithCompletion:(void(^)(NSDictionary*))completion;
-(void)honkCarHornWithCompletion:(void(^)(NSDictionary*))completion;
-(void)getLockStatusOfCarWithCompletion:(void(^)(NSDictionary*))completion;
-(void)lockCarWithCompletion:(void(^)(NSDictionary*))completion;
-(void)flickCarHeadlightsWithCompletion:(void(^)(NSDictionary*))completion;

+(instancetype)sharedInstance;

@end
