//
//  VinTable.h
//  SmartCarProj
//
//  Created by Siddhant Dange on 4/2/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface VinRowType : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *vinLabel;


@end
