//
//  InterfaceController.m
//  SmartCarProj WatchKit Extension
//
//  Created by Siddhant Dange on 3/27/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "SettingsController.h"
#import "VinRowType.h"
#import "User.h"


@interface SettingsController()
@property (weak, nonatomic) IBOutlet WKInterfaceTable *vinTable;

@end


@implementation SettingsController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];

    // Configure interface objects here.
    User *user = [User sharedInstance];
    [_vinTable setNumberOfRows:user.vins.count withRowType:@"VinRowType"];
    for (NSInteger i = 0; i < _vinTable.numberOfRows; i++) {
        VinRowType* theRow = [_vinTable rowControllerAtIndex:i];
        NSString *vin = [user vins][i];
        [theRow.vinLabel setText:vin];
    }
}

-(void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex{
    
    User *user = [User sharedInstance];
    NSString *selectedVin = [user vins][rowIndex];
    [user selectVin:selectedVin];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



