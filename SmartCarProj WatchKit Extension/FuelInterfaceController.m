//
//  FuelInterfaceController.m
//  SmartCarProj
//
//  Created by Siddhant Dange on 4/6/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "FuelInterfaceController.h"
#import "User.h"


@interface FuelInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *batteryLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *fuelLabel;
@property (weak, nonatomic) IBOutlet WKInterfaceSlider *batterySlider;
@property (weak, nonatomic) IBOutlet WKInterfaceSlider *fuelSlider;

@end


@implementation FuelInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    // Configure interface objects here.
    User *user = [User sharedInstance];
    [user getCarFuelLevelWithCompletion:^(NSDictionary *data) {
        
        [_batterySlider setHidden:YES];
        [_batteryLabel setHidden:YES];
        [_fuelSlider setHidden:YES];
        [_fuelLabel setHidden:YES];
        
        if([data.allKeys containsObject:@"battery_percentage"]){
            NSNumber *batteryPerc = data[@"battery_percentage"];
            
            [_batterySlider setValue:batteryPerc.floatValue];
            
            [_batterySlider setHidden:NO];
            [_batteryLabel setHidden:NO];
        }
        
        
        if([data.allKeys containsObject:@"fuel_percentage"]){
            NSNumber *fuelPerc = data[@"fuel_percentage"];
            [_fuelSlider setValue:fuelPerc.floatValue];
            
            [_fuelSlider setHidden:NO];
            [_fuelLabel setHidden:NO];
        }
    }];
    
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



