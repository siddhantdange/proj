//
//  ActionInterfaceController.m
//  SmartCarProj
//
//  Created by Siddhant Dange on 4/6/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "ActionInterfaceController.h"
#import "User.h"


@interface ActionInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceSwitch *lockSwitch;
@property (nonatomic, strong) User *user;

@end


@implementation ActionInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
    [_user getLockStatusOfCarWithCompletion:^(NSDictionary *data) {
        BOOL lockStatus = ((NSNumber*)data[@"lock_status"]).boolValue;
        [_lockSwitch setOn:lockStatus];
    }];
}

- (IBAction)lockSwitched:(BOOL)value {
    if(value){
        [_lockSwitch setEnabled:NO];
        [_user lockCarWithCompletion:^(NSDictionary *data) {
            [_lockSwitch setEnabled:YES];
        }];
    }
}

- (IBAction)flickHeadlight {
    [_user flickCarHeadlightsWithCompletion:nil];
}
- (IBAction)honkHorn {
    [_user honkCarHornWithCompletion:nil];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    _user = [User sharedInstance];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
}

@end



