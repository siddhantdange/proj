//
//  SecondInterfaceController.m
//  SmartCarProj
//
//  Created by Siddhant Dange on 3/29/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "CarLocationInterfaceController.h"
#import "User.h"


@interface CarLocationInterfaceController()
@property (weak, nonatomic) IBOutlet WKInterfaceMap *mapView;
@property (nonatomic, strong) NSTimer *timer;

@end


@implementation CarLocationInterfaceController

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    
}

-(void)updateLocation{
    
    //grab coordinate info
    User *user = [User sharedInstance];
    
    [user getCarLocationWithCompletion:^(NSDictionary *locationData) {
        float lat =  ((NSString*)locationData[@"lat"]).floatValue;
        float lon =  ((NSString*)locationData[@"lon"]).floatValue;
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
        MKCoordinateSpan span = MKCoordinateSpanMake(.007f, .007f);
        MKCoordinateRegion region = MKCoordinateRegionMake(coordinate, span);
        [_mapView setRegion:region];
        [_mapView addAnnotation:coordinate withPinColor:WKInterfaceMapPinColorRed];
    }];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateLocation) userInfo:nil repeats:YES];
    //[_timer fire];
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
    
    [_timer invalidate];
    _timer = nil;
}

@end



