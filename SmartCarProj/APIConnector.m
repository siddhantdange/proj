//
//  APIConnector.m
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "APIConnector.h"
#import "User.h"

#define BASE_URL @"http://bmw.beta.smartcar.com/"

@implementation APIConnector

#pragma -mark Request

-(void)requestMethod:(NSString*)method withPath:(NSString*)path andData:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", BASE_URL, path]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    request.HTTPMethod = method;
    
    if(data){
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        [request setHTTPBody:jsonData];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        //JSON array or object
        NSError *jsonError = nil;
        NSDictionary *resultDict = @{};
        if(data){
            resultDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completion){
                completion(resultDict);
            }
        });
    }];
}

-(NSString*)stringFromDict:(NSDictionary*)dict{
    NSMutableString *data = @"".mutableCopy;
    for(NSObject *key in dict){
        [data appendFormat:@"&%@=%@",key,dict[key]];
    }
    
    return data;
}

@end
