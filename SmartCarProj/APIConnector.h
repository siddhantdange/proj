//
//  APIConnector.h
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIConnector : NSObject

-(void)requestMethod:(NSString*)method withPath:(NSString*)path andData:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion;

@end
