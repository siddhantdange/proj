//
//  ViewController.m
//  SmartCarProj
//
//  Created by Siddhant Dange on 3/27/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()

@property (nonatomic, strong) NSMutableArray *vins;
@property (weak, nonatomic) IBOutlet UITableView *vinTable;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    //load vins stored in memory
    NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.SmartCarProj"];
    [mySharedDefaults synchronize];
    _vins = [NSArray arrayWithArray:[mySharedDefaults arrayForKey:@"vins"]].mutableCopy;
    
    if(!_vins){
        _vins = @[].mutableCopy;
    }
}

//alert to enter in new vin
- (IBAction)addVin:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add VIN" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"VIN";
    }];
    UIAlertAction *addAction = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        UITextField *vinField = alert.textFields[0];
        NSString *vin = vinField.text;
        [_vins addObject:vin];
        
        //store in defaults on background thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUserDefaults *mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.SmartCarProj"];
            [mySharedDefaults setObject:_vins forKey:@"vins"];
            [mySharedDefaults synchronize];
            });
        
        [_vinTable reloadData];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [alert addAction:addAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma -mark UITableViewDataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int numRows = (int)_vins.count;
    return numRows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"cellIdentifier";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] init];
    }
    
    cell.textLabel.text = _vins[indexPath.row];
    
    return cell;
}

#pragma -mark UITableViewDelegate Methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
