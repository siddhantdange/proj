//
//  AppDelegate.h
//  SmartCarProj
//
//  Created by Siddhant Dange on 3/27/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

